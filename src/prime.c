#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]);
int prime_default(int n);

int main(int argc, char *argv[])
{
  int n;
  int n_factor;
  int n_hi;
  int n_lo;
  int primes;

  n_lo = 1;
  n_hi = 131072;
  n_factor = 2;

  printf("\n");
  printf("                           Default \n");
  printf("         N     Pi(N)          Time \n");
  printf("\n");

  n = n_lo;

  while (n <= n_hi) {
    primes = prime_default(n);

    printf("  %8d  %8d\n", n, primes);

    n = n * n_factor;
  }
  /*
    Terminate.
  */
  printf("\n");
  printf("  Normal end of execution.\n");

  return 0;
}


/*
  Purpose:
   counts primes.
  Licensing:
    This code is distributed under the GNU LGPL license.
  Modified:
    10 July 2010
  Author:
    John Burkardt
  Parameters:
    Input, the maximum number to check.
    Output, the number of prime numbers up to N.
*/
int prime_default(int n)
{
  int i;
  int j;
  int prime;
  int total = 0;

  for (i = 2; i <= n; i++) {
    prime = 1;

    for (j = 2; j < i; j++) {
      if (i % j == 0) {
        prime = 0;
        break;
      }
    }
    total = total + prime;
  }

  return total;
}
